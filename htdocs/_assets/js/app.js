/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/_assets/js/index.js","commons~main"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/_assets/js/index.js":
/*!*********************************!*\
  !*** ./src/_assets/js/index.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jquery_easing__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery.easing */ "./node_modules/jquery.easing/jquery.easing.js");
/* harmony import */ var jquery_easing__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_easing__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var bxslider_dist_jquery_bxslider_min__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! bxslider/dist/jquery.bxslider.min */ "./node_modules/bxslider/dist/jquery.bxslider.min.js");
/* harmony import */ var bxslider_dist_jquery_bxslider_min__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(bxslider_dist_jquery_bxslider_min__WEBPACK_IMPORTED_MODULE_2__);



var modePC = false;
var modeSP = false;
var isTouchUA = false;
var isClickUA = false;
var isAndroidUA = false;
var os_windows = false;
var ua = window.navigator.userAgent.toLowerCase();
var content = "width=device-width, initial-scale=1";

if (ua.indexOf("windows nt") !== -1) {
  os_windows = true;
}

if (ua.indexOf("iphone") != -1 || ua.indexOf("ipod") != -1 || ua.indexOf("blackberry") != -1 || ua.indexOf("iemobile") != -1) {
  isTouchUA = true;
  modeSP = true;

  if (window.innerWidth > window.innerHeight) {
    content = "width=1100";
  }
} else if (ua.indexOf("android") != -1 && ua.indexOf("mobile") != -1) {
  isTouchUA = true;
  modeSP = true;

  if (window.innerWidth > window.innerHeight) {
    content = "width=1100";
  }
} else if (ua.indexOf("ipad") != -1 || ua.indexOf("android") != -1) {
  content = "width=1350";

  window.onresize = function () {
    var scale = window.innerWidth / 1100;
    var style = document.body.style;
    style.zoom = scale;
  };

  window.onload = function () {
    var scale = window.innerWidth / 1100;
    var style = document.body.style;
    style.zoom = scale;
  };

  isTouchUA = true;
  modePC = true;
} else {
  isClickUA = true;
  modePC = true;
}

if (content) {
  var metaDiscre = document.head.children;
  var metaLength = metaDiscre.length;

  for (var i = 0; i < metaLength; i++) {
    var proper = metaDiscre[i].getAttribute('name');

    if (proper === 'viewport') {
      var dis = metaDiscre[i];
      dis.setAttribute('content', content);
    }
  }
}

if (os_windows) {
  var body_elements = document.getElementsByTagName('body');
  body_elements[0].classList.add('os_win');
}

var current_scroll_top = 0;
var orientation_landscape = false;

if (window.innerWidth > window.innerHeight) {
  orientation_landscape = true;
}

set_sidebg_size();
$(window).on('load', function () {
  set_sidebg_size();
  top_silder_init();
  recommended_articles_silder_init();
  set_detail_bg();
  set_detail_bg_size();
  seminar_scroll();
  set_gotobut_aty();
});
$(window).on('resize', function () {
  resize_mb();
  set_sidebg_size();
  recommended_slide_resize();
  set_detail_bg_size();
});
var startPos = 0;
var winScrollTop = 0;
$(window).on('scroll', function () {
  winScrollTop = $(this).scrollTop();
  var ww = $(window).width();

  if ($('body').hasClass('spmn')) {
    $('body,html').scrollTop(current_scroll_top);
  }

  if (winScrollTop > 150) {
    if (!$('body').hasClass('fxhm')) {
      $('body').addClass('fxhm');
    }
  } else {
    if ($('body').hasClass('fxhm')) {
      $('body').removeClass('fxhm');
    }
  }

  if (winScrollTop > 350) {
    if (ww > 769) {
      if (!$('body').hasClass('vdhm')) {
        $('body').addClass('vdhm');
      }
    } else {
      if (winScrollTop >= startPos) {
        if ($('body').hasClass('vdhm')) {
          $('body').removeClass('vdhm');
        }
      } else {
        if (!$('body').hasClass('vdhm')) {
          $('body').addClass('vdhm');
        }
      }
    }
  } else {
    if ($('body').hasClass('vdhm')) {
      $('body').removeClass('vdhm');
    }
  }

  startPos = winScrollTop;
});

function resize_mb() {
  if (modeSP) {
    if (window.innerWidth > window.innerHeight) {
      if (!orientation_landscape) {
        orientation_landscape = true;
        setTimeout(doReload, 500);
      }
    } else {
      if (orientation_landscape) {
        orientation_landscape = false;
        setTimeout(doReload, 500);
      }
    }
  }
}

function doReload() {
  window.location.reload();
}

function set_sidebg_size() {
  var ww = $(window).width();
  var side_bg_size = 300 + (ww - 1050) / 2;
  $("#side-bg-style").remove();
  $('head').append('<style id="side-bg-style">.content-top-wrap:before { width: ' + side_bg_size + 'px; opacity: 1;} </style>');
}

$('.go-top-but').on('click', function () {
  $('body, html').animate({
    scrollTop: 0
  }, 1000);
});
$('.sp-trig a').on('click', function () {
  toggle_spnav();
  return false;
});
$('.anker-link a').on('click', function () {
  var tgt = $(this).attr('href');
  var tgt_y = $(tgt).offset().top;
  $('body, html').animate({
    scrollTop: tgt_y
  }, 500);
  return false;
});
$('.article-wrap dl dd a').on('click', function () {
  var tgt = $(this).attr('href');
  var tgt_y = $(tgt).offset().top;
  tgt_y = tgt_y - 100;
  $('body, html').animate({
    scrollTop: tgt_y
  }, 500);
  return false;
});

function toggle_spnav() {
  if ($('body').hasClass('spmn')) {
    $('body').removeClass('spmn');
  } else {
    $('body').addClass('spmn');

    if (!$('body').hasClass('vdhm')) {
      $('body').addClass('vdhm');
    }

    current_scroll_top = $(window).scrollTop();
  }
}

function set_detail_bg() {
  if ($('.detail-bg').length) {
    var tgt_url = $('.main-image img').attr('src');
    tgt_url = 'url(' + tgt_url + ')';
    $('.detail-bg').css('background-image', tgt_url);
  }
}

function set_detail_bg_size() {
  if ($('.detail-bg').length) {
    var ww = $(window).width();

    if (ww > 769) {
      var set_detail_bg_size = 750 + (ww - 1050) / 2;
      $('.detail-bg').width(set_detail_bg_size);
    } else {
      $('.detail-bg').width('100%');
    }
  }
}
/* TOP MV SLIDER */


var top_mv_slide = null;
var top_mv_slide_move = false;

function top_silder_init() {
  if ($('.top-mv-slider').length) {
    top_mv_slide = $('.top-mv-slider .slider').bxSlider({
      auto: true,
      pagerSelector: '.top-mv-pager',
      useCSS: true,
      easing: 'swing',
      speed: 1200,
      pause: 5000,
      touchEnabled: false,
      onSlideBefore: function onSlideBefore() {
        top_silder_set_info();
        top_mv_slide_move = true;
        this.stopAuto();
      },
      onSlideAfter: function onSlideAfter() {
        top_mv_slide_move = false;
        this.startAuto();
      }
    });
  }
}

function top_silder_set_info() {
  var top_mv_text = "";
  var current_no = top_mv_slide.getCurrentSlide() + 1;
  top_mv_text = current_no + "/" + top_mv_slide.getSlideCount();
  $('.top-mv-bg .img-no').html(top_mv_text);
}

$('.top-mv-slider .org-prev a').on('click', function () {
  if (!top_mv_slide_move) {
    top_mv_slide.goToPrevSlide();
  }

  return false;
});
$('.top-mv-slider .org-next a').on('click', function () {
  if (!top_mv_slide_move) {
    top_mv_slide.goToNextSlide();
  }

  return false;
});
$(document).on('mousedown', '.top-mv-slider .bx-wrapper a', function () {
  var $this = $(this),
      myHref = $this.attr('href'),
      myTarget = $this.attr('target');

  if (myTarget === '_blank') {
    window.open(myHref);
  } else {
    window.location.href = myHref;
  }
});
var recommended_slide = null;

function recommended_articles_silder_init() {
  var ww = $(window).width();

  if ($('.recommended-articles').length) {
    if (ww > 769) {
      recommended_slide = $('.recommended-articles .article-list.wrap').bxSlider({
        auto: true,
        controls: true,
        pager: false,
        maxSlides: 3,
        minSlides: 3,
        slideWidth: 300,
        slideMargin: 70,
        moveSlides: 1,
        infiniteLoop: true,
        touchEnabled: false,
        onSlideBefore: function onSlideBefore() {
          this.stopAuto();
        },
        onSlideAfter: function onSlideAfter() {
          this.startAuto();
        }
      });
    }
  }
}

function recommended_slide_resize() {
  var ww = $(window).width();

  if ($('.recommended-articles').length) {
    if (ww > 769) {
      if (recommended_slide == null) {
        recommended_slide = $('.recommended-articles .article-list.wrap').bxSlider({
          auto: true,
          controls: true,
          pager: false,
          maxSlides: 3,
          minSlides: 3,
          slideWidth: 300,
          slideMargin: 70,
          touchEnabled: false,
          onSlideBefore: function onSlideBefore() {
            this.stopAuto();
          },
          onSlideAfter: function onSlideAfter() {
            this.startAuto();
          }
        });
      }
    } else {
      if (recommended_slide != null) {
        recommended_slide.destroySlider();
        recommended_slide = null;
      }
    }
  }
}


var method_ds = false;

if ($('.method-ds-wrap').length) {
  method_ds = true;
}

function seminar_scroll() {
  if ($('.seminar-page').length) {
    var urlHash = location.hash;

    if (urlHash) {
      var position = $(urlHash).offset().top;
      position = position - 100;
      $('body, html').animate({
        scrollTop: position
      }, 500);
    }
  }
}

$('a.web-clipper').on('click', function () {
  window.open('https://web-clipper.com/bookmark/add?url=' + encodeURIComponent(location.href));
  return false;
});

function set_gotobut_aty() {
  var docHeight = $(document).height();

  if ($('.main-sub').length) {
    var at_y = $('footer').offset().top - $('.content-top-wrap').next().offset().top + 80;
    $('.go-top-but.at').css('top', '-' + at_y + 'px');
    $('.go-top-but.at').addClass('on');
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ })

/******/ });
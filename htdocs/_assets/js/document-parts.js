
// 追従ナビ
let categoryNav = document.getElementById('js-categoryNav');

categoryNav.addEventListener('mouseover', function(){
  categoryNav.classList.add('is-active');
});

categoryNav.addEventListener('mouseleave', function(){
  categoryNav.classList.remove('is-active');
});

let footer = document.querySelector('footer');
footer.id = 'footer';

window.onload = function() {
  let windowHeight = window.innerHeight; // windowの高さ
  let popularPos = document.querySelector('.popular-article-list').getBoundingClientRect().top + window.pageYOffset; // 止める場所の次の要素のスクロール位置取得
  let footerPos = document.querySelector('footer').getBoundingClientRect().top + window.pageYOffset;
  let topBtn = document.querySelector('.go-top-but');

  window.addEventListener('scroll', function(){
    // 上からのスクロール値
    let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;

    // 上からのスクロール値 > .popular-article-listの位置 - 画面の高さ
    if(scrollTop > (popularPos + 74) - windowHeight) {
      categoryNav.classList.add('is-absolute');
    } else {
      categoryNav.classList.remove('is-absolute');
    }

    if(scrollTop > 200) {
      topBtn.style.setProperty('opacity', '1');
    } else {
      topBtn.style.setProperty('opacity', '0');
    }

    if(scrollTop > footerPos - windowHeight) {
      topBtn.classList.add('is-absolute');
    } else {
      topBtn.classList.remove('is-absolute');
    }
  });
}

let anchorTrigger = document.getElementById('js-anchorTrigger');
let anchorTarget = document.getElementById('js-anchorTarget');

anchorTrigger.addEventListener('mouseover', function(){
  anchorTarget.classList.add('is-active');
});

anchorTrigger.addEventListener('mouseleave', function(){
  anchorTarget.classList.remove('is-active');
});


// アンカーリンク(Page Top)
let target = document.querySelector('body');
target.setAttribute('id', 'top');
